/**
 * Created by user on 2018/2/27.
 */
import template from './index.html';
import style from "./index.scss";
import actions from '@/js/redux/actions';
import angular from "angular";
// import injector from '@/js/injector';
// import utils from '@/js/utils';
import redux from '@/js/redux';
const name='pageIndexHome';
@redux
class controller{
  constructor() {
    this.style = style['panel-app'];
    this.firstName = 'zhang';
    this.lastName = 'tony';
    this.lists = [
      {name:'a'},
      {name:'b'},
      {name:'c'},
      {name:'d'}
    ];
  }


  getName() {
    return this.name;
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  $onInit() {

  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }

}

export default angular
  .module(name, [])
  .component(name,{
    template,
    controllerAs: "m",
    controller,
    bindings: {
      year: '<',
      month: '<'
    }
  })
  .name;

