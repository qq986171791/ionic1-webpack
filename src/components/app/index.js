/**
 * Created by user on 2018/2/27.
 */
import template from './index.html';
import $routeConfig from "@/js/route";
import style from "./index.scss";
import angular from "angular";
import pageIndexHome from '@/components/pageIndexHome';
import pageLoginHome from '@/components/pageLoginHome';
const name='app';
class controller{
  constructor() {
    this.style = style['panel-app'];
    this.name = 'angular&es6';
    this.firstName = 'zhang';
    this.lastName = 'tony';
  }

  getName() {
    return this.name;
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  $onInit() {

  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }
}

export default angular
  .module(name, [
    pageIndexHome,
    pageLoginHome
  ])
  .component(name,{
    template,
    controllerAs: "m",
    controller,
    $routeConfig,
    bindings: {
      year: '<',
      month: '<'
    }
  })
  .name;

