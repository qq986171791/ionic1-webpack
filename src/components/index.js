/**
 * Created by user on 2018/3/1.
 */

import app from './app';
import pageIndexHome from './pageIndexHome';
import pageLoginHome from './pageLoginHome';

export default angular
  .module('starHome.components', [
    app,
    pageIndexHome,
    pageLoginHome
  ])
  .name;
