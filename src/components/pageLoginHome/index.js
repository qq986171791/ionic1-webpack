/**
 * Created by user on 2018/2/27.
 */
import template from './index.html';
import style from "./index.scss";
import angular from "angular";
const name = 'pageLoginHome';
class controller{
  constructor() {
    this.style = style['panel-app'];
    this.firstName = 'zhang';
    this.lastName = 'tony';
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  $onInit() {

  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }
}
export default angular
  .module(name, [])
  .component(name,{
    template,
    controllerAs: "m",
    controller,
    bindings: {

    }
  })
  .name;


