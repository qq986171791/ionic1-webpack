export const SELECT_DEVICE = 'SELECT_DEVICE'

export function selectDevice(params) {
  return {
    type: SELECT_DEVICE,
    params
  }
}

export default {
  selectDevice
}
