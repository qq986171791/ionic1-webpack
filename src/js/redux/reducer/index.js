import { combineReducers } from 'redux';

import store from '../store';

import { SELECT_DEVICE } from '../actions'

export default function reducer(state = store, action) {
  let strategy = {
    [SELECT_DEVICE](){
      return Object.assign({}, state, {
        list: action.params
      })
    }
  };
  return strategy[action.type] && strategy[action.type]();
}
