/**
 * Created by user on 2018/3/1.
 */
import actions from '@/js/redux/actions';
import injector from '@/js/injector';

function decorateBeforeFn(bFn,fn) {
  return function (...args) {
    bFn.apply(this,[this]);
    return fn.apply(this,args);
  }
}

export default function (target, key, descriptor) {
  let $onInit = target.prototype.$onInit;
  let $onDestroy = target.prototype.$onDestroy;
  let $ngRedux = null;
  let disconnect = null;
  target.prototype.$onInit = decorateBeforeFn(function () {
    $ngRedux = injector.get('$ngRedux');
    disconnect = $ngRedux.connect((state = {}) => {
      if(!state){
        return state;
      }
      return {
        list: state.list
      }
    }, actions)(this);
  },$onInit);
  target.prototype.$onDestroy = decorateBeforeFn(() => {
    disconnect();
  },$onDestroy);
  return target;
}
