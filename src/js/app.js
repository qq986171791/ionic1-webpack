// Ionic Starter App
import '@/css/main.scss';
import {ngComponentRouterModule} from '@/lib/angularRouter'
import {ionicApp} from "@/js/ionicApp";
require('@/lib/ionic/js/ionic.bundle.js');
console.log(angular);
import register from "@/js/register";
import ngRedux from 'ng-redux';
import createLogger from 'redux-logger'
import reducer from '@/js/redux/reducer'
// import ngHttpMiddleware from '@/js/middleware/http';
// import components from '@/components';


// 理想状态下，对于一个业务系统而言，会用到angular语法只有
// angular.controller、
// angular.component
// angular.directive、
// angular.config
// 这几种。其他地方我们都可以实现成框架无关的。


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


let module = angular.module('App',
  [
  	'ionic',
    ngComponentRouterModule,
    // ngRedux,
    // ngHttpMiddleware,
    // components
  ]).run();

// module.value("$routerRootComponent", "app");

// register.module(module);

// register.config(['$ngReduxProvider',($ngReduxProvider) => {
//   $ngReduxProvider.createStoreWith(reducer,[createLogger]);
// }])
