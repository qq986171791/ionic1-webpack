/**
 * Created by user on 2018/2/28.
 */
let bus = (function () {
  let list = {},
    on,
    trigger,
    off;
  on = function (type,fn) {
    if(!list[type]){
      list[type] = [];
    }
    list[type].push(fn);
  };

  trigger = function () {
    let type = Array.prototype.shift.call(arguments),
      fns = list[type];
    if(!fns || fns.length === 0){
      return false;
    }
    for(let i = 0,fn;fn = fns[i++];){
      fn.apply(this,arguments);
    }
  };

  off = function (type,fn) {
    let fns = list[type];
    if(!fns){
      return false;
    }
    if(!fn){
      fns && (fns.length = 0);
    }else{
      for(let l = fns.length - 1; l >= 0;l--){
        let _fn = fns[l];
        if(_fn === fn){
          fns.splice(l,1);
        }
      }
    }
  };

  return {
    on:on,
    trigger:trigger,
    off:off
  };
})();
export default bus;
