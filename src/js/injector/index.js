/**
 * Created by user on 2018/2/28.
 */
export default {
    get(service){
        return angular.element(document.querySelector('[ng-app]')).injector().get(service);
    }
};