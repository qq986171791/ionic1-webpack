/**
 * Created by user on 2018/2/27.
 */
import injector from '@/js/injector';
import {FetchHttp} from 'es6-http-utils';
export default {
  get(url) {
      return injector.get('$http').get(url);
  },

  save(url, payload) {
    return FetchHttp.post(url, payload);
  }
}
