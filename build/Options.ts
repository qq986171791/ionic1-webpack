/**
 * Created by user on 2018/1/18.
 */
import {Factory} from "./oop/Factory";
import {webpackConfig} from "./webpack.config";
import {Utils} from "./Utils";
export class Options{
    private port = 8888;
    public options: any = {
        evn:{
            dev:true,
            hmr:false,
            min:false,
            port:this.port,
            publicPath:`http://localhost:${this.port}/`
        },
        webpackDevServer:{
            contentBase: __dirname+'/www',
            hot: false,
            host: 'localhost',
        },
        webpackConfig:webpackConfig
    };
    protected opts = {};
    constructor(){
        let webpackDevServer = this.getOpts('webpackDevServer');
        let webpackConfigOpts = this.getOpts('webpackConfig');
        let evnOpts = this.getOpts('evn');
        Object.defineProperty(this.options.evn,'dev',{
            set:(v) => {
                webpackDevServer.hot = v;
                if(v){
                    Utils.extend(webpackConfigOpts,{
                        output: {
                            publicPath: evnOpts.publicPath
                        }
                    })
                }
            }
        })
        Object.defineProperty(this.options.evn,'hmr',{
            set:(v) => {
                webpackDevServer.hot = v;
                if(v){
                    Utils.extend(webpackConfigOpts,{
                        output: {
                            publicPath: evnOpts.publicPath
                        }
                    })
                }
            }
        })
        Object.defineProperty(this.options.evn,'min',{
            set:(v) => {
                webpackDevServer.hot = v;
            }
        })
    }
    public setOpts(key: string,v: any): any{
        return this.options[key] = v;
    }
    public getOpts(key: string): any{
        return this.options[key];
    }
    public c(key: string,k: string, v?: any): any{
        this.opts = this.options[key];
        if(!v){
            return this.opts[k];
        }
        return this.opts[k] = v;
    }
}

Factory.setIns('Options',Options);

