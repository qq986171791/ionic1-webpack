/**
 * Created by user on 2018/1/18.
 */
import {EhInit, EhBeforeInit} from "./oop/intface";
import {Factory} from "./oop/Factory";
import {Utils} from "./Utils";
import {Options} from "./Options";
import {Dev} from "./cmd/Dev";
import {Hmr} from "./cmd/Hmr";
import {Build} from "./cmd/Build";
import {Input} from "./cmd/Input";

export class Eh implements EhInit,EhBeforeInit{
    private ehBeforeInitO: any = {};
    private ehInitO: any = {
        'Options':Options,
        'Dev':Dev,
        'Hmr':Hmr,
        'Build':Build,
        'Input':Input,
    };

    constructor(){
        this.ehBeforeInit();
        this.ehInit();
    }
    ehBeforeInit(){
        Utils.each(this.ehBeforeInitO,(item,key) => {
            Factory.getIns(key).ehBeforeInit();
        });
    }
    ehInit(){
        Utils.each(this.ehInitO,(item,key) => {
            let ins: any = null;
            let params = [];
            ins = Factory.getIns(key,params);
            ins.ehInit && ins.ehInit();
        });
    }
}
let eh = new Eh();