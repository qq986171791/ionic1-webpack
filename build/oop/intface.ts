/**
 * Created by user on 2018/1/18.
 */
export interface EhBeforeInit{
    ehBeforeInit();
}

export interface EhInit{
    ehInit();
}

export interface EhAfterInit{
    ehAfterInit();
}

export interface EhOnInputCmd{
    ehOnInputDev(cmd:string);
    ehOnInputBuild(cmd:string);
    ehOnInputHmr(cmd:string);
}

