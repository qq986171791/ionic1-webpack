/**
 * Created by user on 2018/1/18.
 */

export class Factory{
    public static strategy: any = {

    };
    private static insList: any = {};
    static getIns(ins: string,params?: Array = []): any{
        return Factory.insList[ins] = Factory.insList[ins] || new Factory.strategy[ins](...params);
    }
    static setIns(key: string,ins: any){
        Factory.strategy[key] = Factory.strategy[key] || ins;
    }
}