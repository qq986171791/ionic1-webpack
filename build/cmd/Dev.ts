/**
 * Created by user on 2018/1/19.
 */
import intface from "../oop/intface";
import {Factory} from "../oop/Factory";
import {EhOnInputCmd} from "../oop/intface";
import {EhInit} from "../oop/intface";
import webpackDevServer = require("webpack-dev-server");
import webpack = require('webpack');
const Options = Factory.getIns('Options');
import {Utils} from "../Utils";

export class Dev implements EhInit{
    private opts: any = Factory.getIns('Options').getOpts('evn');
    constructor(){

    }
    ehInit(){

    }
    ehOnInputDev(){
        this.opts.dev = true;
        this.opts.hmr = false;
        this.opts.min = false;
        const webpackConfig = Options.getOpts('webpackConfig');
        Utils.extend(webpackConfig,{
            plugins:[
                new webpack.DefinePlugin({
                    'process.env.NODE_ENV': JSON.stringify('development')
                }),
            ]
        },true);
        const webpackDevServerConfig = Options.getOpts('webpackDevServer');
        webpackDevServer['addDevServerEntrypoints'](
                webpackConfig,
                webpackDevServerConfig
            );
        let compiler = webpack(webpackConfig);
        let server = new webpackDevServer(compiler, webpackDevServerConfig);
        server.listen(this.opts.port, 'localhost', () => {
            console.log(`dev server listening on port ${this.opts.port}`);
        });
    }
}
Factory.setIns('Dev',Dev);