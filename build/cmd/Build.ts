/**
 * Created by user on 2018/1/19.
 */
import {Utils} from "../Utils";
import {Factory} from "../oop/Factory";
import webpackDevServer = require("webpack-dev-server");
import webpack = require('webpack');
const Options = Factory.getIns('Options');
import UglifyJSPlugin = require('uglifyjs-webpack-plugin');
export class Build{
    private opts: any = Factory.getIns('Options').getOpts('evn');
    ehOnInputBuild(){
        this.opts.dev = false;
        this.opts.hmr = false;
        this.opts.min = true;
        const webpackConfig = Options.getOpts('webpackConfig');
        Utils.extend(webpackConfig,{
            plugins:[
                new webpack.DefinePlugin({
                  "process.env": {
                    NODE_ENV: JSON.stringify("production")
                  }
                })
            ]
        },true);
        const compiler = webpack(webpackConfig);
        compiler.run((err, stats) => {
            // ...
        });
    }
}
Factory.setIns('Build',Build);
