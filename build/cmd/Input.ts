/**
 * Created by user on 2018/1/19.
 */
import {EhOnInputCmd} from "../oop/intface";
import {Factory} from "../oop/Factory";
import * as commander from "commander";
import EhInit from "../oop/intface";
import {Dev} from "../cmd/Dev";
import {Hmr} from "../cmd/Hmr";
import {Build} from "../cmd/Build";
// 开发模式
// 输出 不压缩 运行

//热更新模式
// 不输出 不压缩 无需重新编译

//发布模式
// 输出 压缩 去除dist

export class Input implements EhInit,EhOnInputCmd{
    constructor(){

    }
    ehInit(){
        this.addListen();
    }
    addListen(){
        commander.arguments('[params]').action((params) => {
            let strategy: any = {};
            strategy.dev = () =>{
                this.ehOnInputDev('dev');
            }
            strategy.hmr = () =>{
                this.ehOnInputHmr('hmr');
            }
            strategy.build = () =>{
                this.ehOnInputBuild('build');
            }
            let f = strategy[params];
            if(!f){
                console.log("================无此命令=================")
                return;
            }
            f();
        }).parse(process['argv']);
    }
    ehOnInputDev(cmd: string){
        Factory.getIns('Dev').ehOnInputDev('dev');
    }
    ehOnInputBuild(cmd: string){
        Factory.getIns('Build').ehOnInputBuild('build');
    }
    ehOnInputHmr(cmd: string){
        Factory.getIns('Hmr').ehOnInputHmr('hmr');
    }
}
Factory.setIns("Input",Input);