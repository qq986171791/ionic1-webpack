/**
 * Created by user on 2018/1/19.
 */
const fs = require('fs');
const path = require('path');
const ts = require('typescript');

Error.stackTraceLimit = Infinity;

const compilerOptions = JSON.parse(fs.readFileSync(path.join(__dirname, '../tsconfig.json')));

const oldRequireTs = require.extensions['.ts'];

require.extensions['.ts'] = function (m, filename) {
    // If we're in node module, either call the old hook or simply compile the
    // file without transpilation. We do not touch node_modules/**.
    // We do touch `Angular CLI` files anywhere though.
    if (filename.match(/node_modules/)) {
        if (oldRequireTs) {
            return oldRequireTs(m, filename);
        }
        return m._compile(fs.readFileSync(filename), filename);
    }

    // Node requires all require hooks to be sync.
    const source = fs.readFileSync(filename).toString();

    try {
        const result = ts.transpile(source, compilerOptions['compilerOptions']);
        // Send it to node to execute.
        return m._compile(result, filename);
    } catch (err) {
        console.error('Error while running script "' + filename + '":');
        console.error(err.stack);
        throw err;
    }
};
