/**
 * Created by user on 2018/1/16.
 */
import HtmlwebpackPlugin = require('html-webpack-plugin');
import CleanWebpackPlugin = require('clean-webpack-plugin');
import AutoDllPlugin = require('autodll-webpack-plugin');
import UglifyJSPlugin = require('uglifyjs-webpack-plugin');
import webpack = require('webpack');
import ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

import {Utils} from "./Utils";


const src = Utils.resolve('/src');
const dist = Utils.resolve('/www');

const config: webpack.Configuration = {
    entry: {
        app:src + "/js/app.js", //已多次提及的唯一入口文件
    },
    devtool: 'inline-source-map',
    resolve:{
      alias: {
        '@': [src,'/'].join(""),
      }
    },
    stats: {
        // Nice colored output
        colors: true
    },
    module: {
        rules: [
            {
              test: /\.js$/,
              exclude: /(node_modules|bower_components)/,   
              use: {
                loader: 'babel-loader',
                options: {
                  compact: false,
                  presets: ["@babel/env"],
                  plugins: ["transform-decorators-legacy",'transform-node-env-inline']
                }
              }
            },
            {
              test: /\.scss$/,
              exclude: /node_modules/,
              use: [{
                loader: "style-loader" // creates style nodes from JS strings
              }, {
                loader: "css-loader", // translates CSS into CommonJS
                options: {
                  modules: true,
                  localIdentName: '[path][name]__[local]--[hash:base64:5]'
                }
              }, {
                loader: "sass-loader", // compiles Sass to CSS
                options: {
                    sourceMap: true
                }
              }]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.ts?$/,
                use: "babel?presets[]=es2015!ts"
            },
            {
                test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader?name=lib/ionic/css/fonts/[name].[ext]'
            },
            {
                test: /\.(png|jpg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 10000
                    } // Convert images < 10k to base64 strings
                }]
            },
            {
              test: /\.html$/,
              loader: 'html-loader'
            }
        ]
    },
    output: {
        path: dist,
        filename: "bundle-[hash].js",
        publicPath: "./"
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new UglifyJSPlugin({
          sourceMap: true,
        }),
        new HtmlwebpackPlugin({
            inject: true,
            template: src+'/index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeAttributeQuotes: true
            }
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new AutoDllPlugin({
            inject: true, // will inject the DLL bundles to index.html
            filename: '[name]_[hash].js',
            entry: {
                vendor: [
                    src + "/lib/ionic/js/ionic.bundle.min.js"
                ]
            },
            plugins: [new UglifyJSPlugin({
              sourceMap: true
            })]
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
};
export {config as webpackConfig};

