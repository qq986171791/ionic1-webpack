/**
 * Created by user on 2018/1/5.
 */
var options = {
    ip:{

    }
};
var factory = {};

factory.strategy = {};

factory.insList = {};

factory.getIns = function (ins) {
    return factory.insList[ins] = factory.insList[ins] || new factory.strategy[ins]();
}

factory.strategy.IP = function () {
    var optsKey = 'ip';
    var opts = options[optsKey];
    var deps = {
        plugins:{ //插件
            devip:require('dev-ip')
        },
        service:{ //服务

        }
    };
    var ins = {
        init:function () {
            ins.initIp();
        },
        setLocal:function (v) {
            opts.local = v;
        },
        getLocal:function () {
            return opts.local;
        },
        initIp:function () {
            var devip = deps.plugins.devip;
            ins.setLocal(devip()[0]);
        }
    };
    ins.init();
    return ins;
}
var ip = factory.getIns('IP');

module.exports = ip;
